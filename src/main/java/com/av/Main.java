package com.av;

import java.security.SecureRandom;
import java.util.Random;
import java.util.logging.Logger;

public class Main {
   public static final Logger LOG = Logger.getLogger(Main.class.getName());
   private static final Random RANDOM = new SecureRandom();

   public static void main(String[] args) {
      int[] numbers = new int[10];

      for (int i = 0; i < numbers.length; i++)
         numbers[i] = RANDOM.nextInt(100);

      LOG.info("BEFORE SORT:");
      printArray(numbers);
      insertionSort(numbers);
      LOG.info("AFTER SORT");
      printArray(numbers);

   }

   private static void insertionSort(int[] numbers) {
      // start at the 2nd value, because the first value is already 'sorted'
      for (int i = 1; i < numbers.length; i++) {
         var currentValue = numbers[i]; // copy
         var prevIndex = i - 1;

         // if previous value is greater than current value
         while (prevIndex >= 0 && numbers[prevIndex] > currentValue) {
            // shift the previous value to the right
            numbers[prevIndex + 1] = numbers[prevIndex];
            // walk to the element before previous
            prevIndex--;
         }
         numbers[prevIndex+1] = currentValue;
      }
   }


   private static void printArray(int[] numbers) {
      StringBuilder result = new StringBuilder("[");
      int lastIndex = numbers.length - 1;

      for (int i = 0; i < numbers.length; i++) {
         result.append(numbers[i]);
         if (i != lastIndex)
            result.append(", ");

      }
      result.append("]");

      LOG.info(()-> "numbers=" + result);
   }
}
